<?php

namespace AppBundle\Service;

use AppBundle\Entity\Nuptic;
use AppBundle\Entity\NupticLog;
use AppBundle\Repository\NupticRepository;
use Doctrine\ORM\EntityManager;

/**
 * Date: 18/02/18
 * Time: 11:47
 */
class NupticService
{
    /**
     * @var Nuptic
     */
    private $genericNuptic;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var NupticRepository
     */
    private $nupticRepo;

    /**
     * NupticService constructor.
     * 
     * @param EntityManager $entityManager
     * @param NupticRepository $nupticRepository
     */
    public function __construct(EntityManager $entityManager, NupticRepository $nupticRepository)
    {
        $this->em               = $entityManager;
        $this->nupticRepo       = $nupticRepository;
    }

    /**
     * @param $nuptic_fields
     * @return array
     */
    public function createHistoric($nuptic_fields)
    {
        $nuptic = new Nuptic();
        $nuptic->setSimulatorId($nuptic_fields[Nuptic::SIMULATOR_ID]);
        $nuptic->setNum($nuptic_fields[Nuptic::NUMBER]);
        $nuptic->setDirection($nuptic_fields[Nuptic::DIRECTION]);
        $nuptic->setRoute($nuptic_fields[Nuptic::ROUTE]);

        $this->em->persist($nuptic);
        $this->em->flush();

        if($nuptic_fields[Nuptic::DIRECTION] === Nuptic::EST_DIRECTION)
            $this->createLog($nuptic);

        return array('id' =>$nuptic->getId(), 'last_request' => $nuptic->getNum() == Nuptic::MAXIMUM_NUMBER);
    }

    /**
     * @param $nuptic_simulator_id
     * @return int
     */
    public function getTotalRoute($nuptic_simulator_id)
    {
        $simulator_results  = $this->em->getRepository('AppBundle:Nuptic')->findBy(['simulator_id' => $nuptic_simulator_id]);
        $total_route        = 0;

        /** @var Nuptic $result */
        foreach ($simulator_results as $result) {
            $total_route += $result->getRoute();
        }

        return $total_route;

    }

    /**
     * @param $nuptic_simulator_id
     * @return array|mixed
     */
    public function getMostUsedCardinalPoints($nuptic_simulator_id)
    {
        if($nuptic_simulator_id) {
            $sql = "SELECT direction, count(direction) AS used_cardinal
                    FROM nuptic
                    WHERE simulator_id = '$nuptic_simulator_id'
                    GROUP BY direction
                    ORDER BY used_cardinal DESC
                    LIMIT 1
                    ";

            $cardinalUsed = $this->em->getConnection()->prepare($sql);
            $cardinalUsed->execute();

            return $cardinalUsed->fetch();
        }

        return array();
    }

    /**
     * @param $nuptic_simulator_id
     * @return array
     */
    public function getDirectionRouteUsed($nuptic_simulator_id)
    {
        if($nuptic_simulator_id) {
            return $this->nupticRepo->getRouteUsedByDirectionFromSimulatorId($nuptic_simulator_id);
        }

        return array();
    }

    /**
     * @param $nuptic
     */
    public function createLog($nuptic)
    {
        $nupticLog = new NupticLog();
        $nupticLog->setNuptic($nuptic);

        $this->em->persist($nupticLog);
        $this->em->flush();
    }

    /**
     * @param array $all_parameters
     * @return bool
     */
    public function checkAllParameters($all_parameters)
    {
        if(
        ! $this->checkSimulatorId($all_parameters) ||
        ! $this->checkNumber($all_parameters) ||
        ! $this->checkDirection($all_parameters) ||
        ! $this->checkRoute($all_parameters)
        )
            return false;

        return true;
    }

    /**
     * @param $all_parameters
     * @return bool
     */
    private function checkSimulatorId($all_parameters)
    {
        if(!isset($all_parameters[Nuptic::SIMULATOR_ID]))
            return false;

        return true;
    }

    /**
     * @param $all_parameters
     * @return bool
     */
    private function checkNumber($all_parameters)
    {
        if(!isset($all_parameters[Nuptic::NUMBER]))
            return false;

        $nuptic_number = $all_parameters[Nuptic::NUMBER];

        if(Nuptic::MINIMUM_NUMBER > $nuptic_number || Nuptic::MAXIMUM_NUMBER < $nuptic_number)
            return false;

        return true;
    }

    /**
     * @param $all_parameters
     * @return bool
     */
    private function checkDirection($all_parameters)
    {
        if(!isset($all_parameters[Nuptic::DIRECTION]))
            return false;

        if(!in_array($all_parameters[Nuptic::DIRECTION], Nuptic::ALL_DIRECTIONS))
            return false;

        return true;
    }

    /**
     * @param $all_parameters
     * @return bool
     */
    private function checkRoute($all_parameters)
    {
        if(!isset($all_parameters[Nuptic::ROUTE]))
            return false;

        $nuptic_route = $all_parameters[Nuptic::ROUTE];

        if(Nuptic::MINIMUM_ROUTE > $nuptic_route || Nuptic::MAXIMUM_ROUTE < $nuptic_route)
            return false;

        return true;
    }
}
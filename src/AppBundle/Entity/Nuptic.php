<?php

namespace AppBundle\Entity;

/**
 * Created by PhpStorm.
 * User: tony
 * Date: 18/02/18
 * Time: 12:04
 */
class Nuptic
{
    const SIMULATOR_ID  = 'id_simulador';
    const NUMBER        = 'num';
    const DIRECTION     = 'direccion';
    const ROUTE         = 'recorrido';

    const MINIMUM_NUMBER    = 1;
    const MAXIMUM_NUMBER    = 60;
    const ALL_DIRECTIONS    = ['Norte', 'Sur', 'Este', 'Oeste'];
    const EST_DIRECTION     = 'Este';
    const MINIMUM_ROUTE     = 10;
    const MAXIMUM_ROUTE     = 20;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $simulator_id;

    /**
     * @var int
     */
    private $num;

    /**
     * @var string
     */
    private $direction;

    /**
     * @var int
     */
    private $route;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Nuptic
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getSimulatorId()
    {
        return $this->simulator_id;
    }

    /**
     * @param int $simulator_id
     * @return Nuptic
     */
    public function setSimulatorId($simulator_id)
    {
        $this->simulator_id = $simulator_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @param int $num
     * @return Nuptic
     */
    public function setNum($num)
    {
        $this->num = $num;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     * @return Nuptic
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * @return int
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param int $route
     * @return Nuptic
     */
    public function setRoute($route)
    {
        $this->route = $route;
        return $this;
    }

}
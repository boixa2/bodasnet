<?php
/**
 * Created by PhpStorm.
 * User: tony
 * Date: 18/02/18
 * Time: 12:58
 */

namespace AppBundle\Entity;


class NupticLog
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var Nuptic
     */
    private $nuptic;

    /**
     * NupticLog constructor.
     */
    public function __construct()
    {
        $this->setCreationDate(new \DateTime());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return NupticLog
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param mixed $creationDate
     * @return NupticLog
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * @return Nuptic
     */
    public function getNuptic()
    {
        return $this->nuptic;
    }

    /**
     * @param Nuptic $nuptic
     * @return NupticLog
     */
    public function setNuptic($nuptic)
    {
        $this->nuptic = $nuptic;
        return $this;
    }

}
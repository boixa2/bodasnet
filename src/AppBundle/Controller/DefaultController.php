<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function orvalAction(Request $request)
    {
        $all_parameters         = $request->query->all();
        $nupticService          = $this->get('service.nuptic');
        $nuptic_historic        = array();

        if($nupticService->checkAllParameters($all_parameters)) {
            $nuptic_historic = $nupticService->createHistoric($all_parameters);
        }

        return new JsonResponse(array('data' => $nuptic_historic));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function orvalResultsAction(Request $request)
    {
        $data = array();

        if($request->get('simulator_id')) {
            $nupticService  = $this->get('service.nuptic');
            $simulator_id   = $request->get('simulator_id');
            $total_route    = $nupticService->getTotalRoute($simulator_id);
            $used_cardinal  = $nupticService->getMostUsedCardinalPoints($simulator_id);

            $data = array('total_route' => $total_route, 'cardinal_used' => $used_cardinal);
        }

        return new JsonResponse($data);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function chartDataAction(Request $request)
    {
        $data = array();

        if($request->get('simulator_id')) {
            $data = $this->get('service.nuptic')->getDirectionRouteUsed($request->get('simulator_id'));
        }

        return new JsonResponse($data);
    }
}

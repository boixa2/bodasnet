<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Created by PhpStorm.
 * User: tony
 * Date: 18/02/18
 * Time: 15:49
 */
class NupticRepository extends EntityRepository
{
    /**
     * @param $simulator_id
     * @return array
     */
    public function getRouteUsedByDirectionFromSimulatorId($simulator_id)
    {
        $qb = $this->createQueryBuilder('n')
                ->select('distinct(n.direction) as direction, sum(n.route) as total_route')
                ->where('n.simulator_id = :simulator_id')
                ->groupBy('n.direction')
                ->setParameter('simulator_id', $simulator_id)
                ->getQuery()
        ;

        return $qb->getResult();
    }
}